
public class Temperaturen {

	public static void main(String[] args) {
		String s = "|";
		
		double f1 = 20;
		double f2 = 10;
		double f3 = 0;
		double f4 = 20;
		double f5 = 30;
		double c1 = 28.8889;
		double c2 = 23.3333;
		double c3 = 17.7778;
		double c4 = 6.6667;
		double c5 = 1.1111;
		
		System.out.printf("Fahrenheit "+ "|" +" %2.0s Celsius  \n" , s);
		System.out.printf("-------------------------- \n");
		System.out.printf("%-1.0f %8s %10.2f \n", -f1 , s , -c1);
		System.out.printf("%-1.0f %8s %10.2f \n", -f2 , s , -c2);
		System.out.printf("+%-1.0f %9s %10.2f \n",f3 , s , -c3);
		System.out.printf("+%-1.0f %8s %10.2f \n", f4 , s , -c4);
		System.out.printf("+%-1.0f %8s %10.2f", f5 , s , -c5);
		
	}

}


/*Fahrenheit Celsius
-20 -28.8889
-10 -23.3333
0 -17.7778
20 -6.6667
30 -1.1111 	
		*/