import java.util.Scanner;
public class Steuersatz {
	/* Nach der Eingabe des Nettowertes soll abgefragt werden, ob der erm��igte oder der volle
	Steuersatz angewendet werden soll. Der Anwender entscheidet sich �ber die Eingabe von �j�
	f�r den erm��igten Steuersatz und mit Eingabe von �n� f�r den vollen Steuersatz.
	Anschlie�end soll das Programm den korrekten Bruttobetrag auf dem Bildschirm ausgeben. */
	public static void main(String[] args) {
		Scanner skin = new Scanner(System.in);
		System.out.println("Eingabe Nettowert:");
		double nettowert = skin.nextDouble();
		System.out.println("Vollen Steuersatz oder ermaeigten Steuersatz anwenden");
		System.out.println("Schreibe j f�r ermae�igten Steuersatz und n f�r vollen Steuersatz");
		char value = skin.next().charAt(0);
		if(value == 'j') {
			double kleinerBruttoBetrag = nettowert * 1.07;
			System.out.printf("%.2f", kleinerBruttoBetrag);
		}
		
		else if(value == 'n') {
			double vollerBruttoBetrag = nettowert * 1.19;
			System.out.printf("%.2f", vollerBruttoBetrag);
		}
		else {
			System.out.println("Falscher Buchstabe eingegeben");
		}
		
	}
	
}
