import java.beans.FeatureDescriptor;
import java.util.Locale;
import java.util.Scanner;

class Automat {
	static double regelTarif = 2.90;
	static double kleinGruppenTarif = 23.50;
	static double tagesKarteTarif = 8.60;

	public static void main(String[] args) {
		// Fahrkartenbestellung erfassen
		fahrkartenbestellungErfassen();
	}

	public static double round(double value, int dezimalPoints) {
		double d = Math.pow(10, dezimalPoints);
		return Math.round(value * d) / d;
	}

	public static int anzahlTicketsBestimmen() {
		Scanner ski = new Scanner(System.in);
		System.out.println("Geben Sie die Anzahl der Tickets an: ");

		int anzahlTickets = ski.nextInt();
		while (anzahlTickets > 10 || anzahlTickets < 1) {

			System.out.println("Sie d�rfen nur 1 bis 10 Tickets w�hen.");
			System.out.print("Sie fahren mit 1 Ticket in der Bestellung fort.");
			anzahlTickets = ski.nextInt();
		}
		return anzahlTickets;

	}

	public static double gesammtBetragBestimmen(int anzahlTickets, double tarifKosten) {
		double gesammtBetrag = tarifKosten * anzahlTickets;
		return gesammtBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner ski2 = new Scanner(System.in);
		System.out.println(zuZahlenderBetrag);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			// System.out.println(zuZahlenderBetrag - eingezahlterGesamtbetrag);
			double formatierterBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			// formatierterBetrag =
			System.out.println("Noch zu zahlen: " + String.format("%1.2f Euro", formatierterBetrag));

			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = ski2.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void fahrkartenbestellungErfassen() {
		boolean auswahlAbgeschlossen = false;
		while (auswahlAbgeschlossen == false) {
		Scanner ski = new Scanner(System.in);
		System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:\r\n"
				+ "  Einzelfahrschein Regeltarif AB [2,90 EUR] (0)\r\n"
				+ "  Tageskarte Regeltarif AB [8,60 EUR] (1)\r\n"
				+ "  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (2)");

		int i = ski.nextInt();
		
		
			switch (i) {
			case 0:
				System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (0)");
				int anzahlTicktes = anzahlTicketsBestimmen();
				double zuZahlenderBetrag = gesammtBetragBestimmen(anzahlTicktes, regelTarif);
				fahrkartenBezahlen(zuZahlenderBetrag);
				fahrkartenAusgeben();
				break;
			case 1:
				System.out.println("Tageskarte Regeltarif AB [8,60 EUR]");
				anzahlTicktes = anzahlTicketsBestimmen();
				zuZahlenderBetrag = gesammtBetragBestimmen(anzahlTicktes, tagesKarteTarif);
				double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
				fahrkartenAusgeben();
				rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

				break;

			case 2:
				System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (1)");
				anzahlTicktes = anzahlTicketsBestimmen();
				zuZahlenderBetrag = gesammtBetragBestimmen(anzahlTicktes, kleinGruppenTarif);
				eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
				fahrkartenAusgeben();
				rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
				break;
			case 3:
				System.out.println("Auswahl beenden");
				auswahlAbgeschlossen = true;
				fahrkartenAusgeben();
				break;
			}
		}

	}

	public static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

		if (r�ckgabebetrag > 0.001) {
			System.out.printf("Der R�ckgabebetrag in H�he von " + String.format("%1.2f", r�ckgabebetrag) + " EURO");
			System.out.println(" wird in folgenden M�nzen ausgezahlt:");
			Automat.round(r�ckgabebetrag, 2);

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}

			while (Automat.round(r�ckgabebetrag, 2) >= 0.05)// 5 CENT-M�nzen
			{

				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
		return r�ckgabebetrag;

	}
}
